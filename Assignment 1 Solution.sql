use assignment1;
select * from worker;

insert into worker(worker_id, first_name, last_name, salary, joining_date, department)
values
(1, 'parul','chandra',15000,'2016-06-11 00:00:00','IT'),
(2, 'piyush','chandra',23000,'2016-06-11 00:00:00','HR'),
(3, 'sonal','chandra',24000,'2016-06-11 00:00:00','Accts'),
(4, 'samar','gautam',15000,'2016-06-11 00:00:00','sales');

--ques1:
select first_name as worker_name from worker;

--ques2:
select upper (first_name) from worker;

--ques3:
select distinct department from worker;

--ques4:
select SUBSTRING(first_name, 1,3) from worker;

--ques5:
select charindex (first_name, 'a') from worker where first_name = 'parul';

--ques6:
select RTRIM(first_name) from worker;

--ques7:
select LTRIM(department) from worker;

--ques8:
select distinct department, len(department) from worker;

--ques9:
select REPLACE(first_name,'a','A') from worker;

--ques10:
select CONCAT(trim(first_name),' ' , last_name) as Complete_name from worker;

--ques11:
select * from worker order by first_name asc;

--ques12:
select * from worker order by first_name asc, department desc;

--ques13:
select * from worker where first_name in ('sonal','chandra');

--ques14:
select * from worker where first_name not in ('samar','gautam');

--ques15:
select * from worker where department = 'HR';

--que16:
select * from worker where first_name like '%a%';

--ques17:
select *from worker where first_name like '%a';

--ques18:
select * from worker where first_name like '%h' and len(first_name) = 6
--ques19:
select * from worker where salary between 10000 and 50000;

--ques20:
select * from worker where month(joining_date) =2 And year(joining_date)=2014

--ques21:
 select count (*) from worker where department = 'HR';
